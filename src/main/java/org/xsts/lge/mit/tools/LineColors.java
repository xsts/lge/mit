/*
 * Group : XSTS/LGE
 * Project : Minimalist Image Toolkit
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 *
 * Some code is from Internet, but I cannot find the reference page.
 */
package org.xsts.lge.mit.tools;

import java.awt.Color;

public class LineColors {
    private static Color hexToColor(String hexValue){
        int rgbEncoded = hexToInt(hexValue);
        int blue = rgbEncoded % 256;
        rgbEncoded -= blue;
        rgbEncoded /= 256;
        int green = rgbEncoded % 256;
        rgbEncoded -= green;
        rgbEncoded /= 256;
        int red = rgbEncoded;
        return new java.awt.Color(red, green, blue);
    }

    private static int hexToInt(String hexValue){
        String candidate = hexValue.toLowerCase();
        char[] hexSix = candidate.toCharArray();
        int rgbEncoded = 0;
        for ( char c : hexSix){
            rgbEncoded *= 16;
            int digitValue = hexDigitToInt(c);
            rgbEncoded += digitValue;
        }
        return rgbEncoded;
    }

    private static int hexDigitToInt(char digit){
        if (digit >= 'a' && digit <= 'f') {
            return digit -'a' + 10;
        } else if (digit >= 'A' && digit <= 'F') {
            return digit -'A' + 10;
        } else if (digit >= '0' && digit <= '9'){
            return digit -'0';
        } else {
            return 0;
        }
    }



}
