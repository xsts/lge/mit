/*
 * Group : XSTS/LGE
 * Project : Minimalist Image Toolkit
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.lge.mit.draw;


import java.awt.*;

public abstract class XRectangleBase extends Drawable{
    int top=0;
    int left=0;
    int width=0;
    int height=0;
    Color color;
    int lineWidth=0;

    public XRectangleBase() {
        super();
    }



    public XRectangleBase top(int top) {
        this.top = top;
        return this;
    }

    public XRectangleBase left(int left) {
        this.left = left;
        return this;
    }

    public XRectangleBase width(int width) {
        this.width = width;
        return this;
    }

    public XRectangleBase height(int height) {
        this.height = height;
        return this;
    }

    public XRectangleBase lineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
        return this;
    }

    public XRectangleBase color(Color color) {
        this.color = color;
        return this;
    }
}
