/*
 * Group : XSTS/LGE
 * Project : Minimalist Image Toolkit
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.lge.mit.draw;

public abstract class Drawable {
    public Drawable() {

    }

    public abstract void draw(XCanvas canvas);

}
