/*
 * Group : XSTS/LGE
 * Project : Minimalist Image Toolkit
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.lge.mit.draw;

import java.awt.Graphics2D;
import java.awt.BasicStroke;
import java.awt.geom.Ellipse2D;

public class XDiscEmpty extends XDiscBase{
    public XDiscEmpty() {
        super();
    }

    @Override
    public void draw(XCanvas canvas) {

        Graphics2D g2d = canvas.g2d();
        g2d.setColor(color);
        g2d.setStroke(new BasicStroke(lineWidth));
        g2d.draw(new Ellipse2D.Float(x-radius, y-radius, 2*radius, 2*radius));
    }
}
