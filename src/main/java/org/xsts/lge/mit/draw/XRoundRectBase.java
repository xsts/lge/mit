package org.xsts.lge.mit.draw;

import java.awt.*;

public abstract class XRoundRectBase extends Drawable{

    int top=0;
    int left=0;
    int width=0;
    int height=0;
    int arcWidth=0;
    int arcHeight=0;
    Color color;
    int lineWidth=0;

    public XRoundRectBase() {
        super();
    }

    public XRoundRectBase top(int top) {
        this.top = top;
        return this;
    }

    public XRoundRectBase left(int left) {
        this.left = left;
        return this;
    }

    public XRoundRectBase width(int width) {
        this.width = width;
        return this;
    }

    public XRoundRectBase height(int height) {
        this.height = height;
        return this;
    }

    public XRoundRectBase arcWidth(int arcWidth) {
        this.arcWidth = arcWidth;
        return this;
    }

    public XRoundRectBase arcHeight(int arcHeight) {
        this.arcHeight = arcHeight;
        return this;
    }

    public XRoundRectBase lineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
        return this;
    }

    public XRoundRectBase color(Color color) {
        this.color = color;
        return this;
    }

}
