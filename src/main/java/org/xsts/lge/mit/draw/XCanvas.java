/*
 * Group : XSTS/LGE
 * Project : Minimalist Image Toolkit
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 *
 * The transparent image processing is from the following GitHub repository:
 * https://github.com/dustinmarx/imageutilities/blob/master/src/dustin/examples/images/ImageTransparency.java
 */
package org.xsts.lge.mit.draw;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.awt.image.FilteredImageSource;
import java.awt.image.ImageFilter;
import java.awt.image.ImageProducer;
import java.awt.image.RGBImageFilter;
import java.io.File;
import java.io.IOException;
import java.awt.AlphaComposite;
public class XCanvas {
    int width = 0;
    int height = 0;
    Color transparentLayer = null;

    BufferedImage bimage = null;
    Graphics2D g2d = null;

    public XCanvas() {

    }

    public XCanvas transparentLayer(Color transparentLayer) {
        this.transparentLayer = transparentLayer;
        return this;
    }

    public XCanvas width(int width) {
        this.width = width;
        return this;
    }

    public XCanvas height(int height) {
        this.height = height;
        return this;
    }

    public XCanvas begin() {
        bimage = new BufferedImage(width, height,
                BufferedImage.TYPE_BYTE_INDEXED);
        g2d = bimage.createGraphics();

        if (transparentLayer != null) {
            g2d.setColor(transparentLayer);
            g2d.setStroke(new BasicStroke(1));
            g2d.fill(new Rectangle2D.Double(0, 0, width, height));
        }

        g2d.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);

        return this;
    }

    public XCanvas end() {
        g2d.dispose();

        return this;
    }

    public Graphics2D g2d() {
        return g2d;
    }

    // Following method is written by someone else (see the top of the file
     BufferedImage imageToBufferedImage(Image im) {
        BufferedImage bi = new BufferedImage
                (im.getWidth(null),im.getHeight(null),BufferedImage.TYPE_INT_ARGB);
        Graphics bg = bi.getGraphics();
        bg.drawImage(im, 0, 0, null);
        bg.dispose();
        return bi;
    }

    // Following method is written by someone else (see the top of the file
    public static Image makeColorTransparent(final BufferedImage im, final Color color)
    {
        final ImageFilter filter = new RGBImageFilter()
        {
            public int markerRGB = color.getRGB() | 0xFF000000;

            public final int filterRGB(final int x, final int y, final int rgb)
            {
                if ((rgb | 0xFF000000) == markerRGB)
                {
                    // Mark the alpha bits as zero - transparent
                    return 0x00FFFFFF & rgb;
                }
                else
                {
                    // nothing to do
                    return rgb;
                }
            }
        };

        final ImageProducer ip = new FilteredImageSource(im.getSource(), filter);
        return Toolkit.getDefaultToolkit().createImage(ip);
    }

    public XCanvas save(String filePath, String formatName) {
        try {
            File outputFile = new File(filePath);
            if (transparentLayer != null) {
                final BufferedImage source = bimage;
                final int color = source.getRGB(0, 0);
                final Image imageWithTransparency = makeColorTransparent(source, new Color(color));
                final BufferedImage transparentImage = imageToBufferedImage(imageWithTransparency);
                ImageIO.write(transparentImage, formatName, outputFile);
            } else {
                ImageIO.write(bimage, formatName, outputFile);
            }

        } catch (IOException e) {
            // handle exception
            System.out.println("Cannot create  image file");
        }

        return this;
    }

}
