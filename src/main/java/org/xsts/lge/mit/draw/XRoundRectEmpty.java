package org.xsts.lge.mit.draw;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;

public class XRoundRectEmpty extends XRoundRectBase {

    public XRoundRectEmpty() { super(); }

    @Override
    public void draw(XCanvas canvas) {
        Graphics2D g2d = canvas.g2d();
        g2d.setColor(color);
        g2d.setStroke(new BasicStroke(lineWidth));
        g2d.draw(new RoundRectangle2D.Double(left, top, width, height, arcWidth, arcHeight));

    }
}
