/*
 * Group : XSTS/LGE
 * Project : Minimalist Image Toolkit
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.lge.mit.draw;

import java.awt.Graphics2D;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;

public class XFont extends Drawable {
    int x=0;
    int y=0;
    int size=0;
    Color color;
    String fontName;
    String text;


    public XFont() { super(); }

    @Override
    public void draw(XCanvas canvas) {
        Graphics2D g2d = canvas.g2d();
        Font font = new Font(fontName, Font.BOLD, size);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        int dw = fm.stringWidth(text)/2;
        int dh = fm.getHeight()/2 -  fm.getAscent();



        g2d.setColor(color);
        g2d.drawString(text, x-dw, y - dh);
    }

    public XFont center(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public XFont size(int size) {
        this.size = size;
        return this;
    }

    public XFont color(Color color) {
        this.color = color;
        return this;
    }

    public XFont fontName(String fontName) {
        this.fontName = fontName;
        return this;
    }

    public XFont text(String text) {
        this.text = text;
        return this;
    }
}
