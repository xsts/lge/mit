package org.xsts.lge.mit.draw;

import java.awt.*;
import java.awt.geom.RoundRectangle2D;

public class XRoundRectFill extends XRoundRectBase {

    public XRoundRectFill() { super(); }

    @Override
    public void draw(XCanvas canvas) {
        Graphics2D g2d = canvas.g2d();
        g2d.setColor(color);
        g2d.setStroke(new BasicStroke(lineWidth));
        g2d.fill(new RoundRectangle2D.Double(left, top, width, height, arcWidth, arcHeight));

    }
}