/*
 * Group : XSTS/LGE
 * Project : Minimalist Image Toolkit
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.lge.mit.draw;


import java.awt.Color;


public abstract class XDiscBase extends Drawable{
    int x=0;
    int y=0;
    int radius=0;
    Color color;
    int lineWidth=0;

    public XDiscBase() {
        super();
    }




    public XDiscBase center(int x, int y) {
        this.x = x;
        this.y = y;
        return this;
    }

    public XDiscBase radius(int radius) {
        this.radius = radius;
        return this;
    }

    public XDiscBase lineWidth(int lineWidth) {
        this.lineWidth = lineWidth;
        return this;
    }

        public XDiscBase color(Color color) {
            this.color = color;
            return this;
        }
}
