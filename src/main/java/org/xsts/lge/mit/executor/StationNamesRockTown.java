package org.xsts.lge.mit.executor;

import org.xsts.lge.mit.draw.XCanvas;
import org.xsts.lge.mit.draw.XFont;
import org.xsts.lge.mit.draw.XRectangleBase;
import org.xsts.lge.mit.draw.XRectangleFill;
import org.xsts.lge.mit.draw.XRoundRectEmpty;

import java.awt.*;

public class StationNamesRockTown {

    Color color;
    String tag;
    Color transparentColor = new Color(192,192,192);
    String stationName;
    Color line1Color;
    Color line2Color;
    Color line3Color;


    public static final int PLAQUE_WIDTH = 1500;
    public static final int PLAQUE_HEIGHT = PLAQUE_WIDTH/8;
    public static final int FONT_SIZE = PLAQUE_HEIGHT * 3/5;
    public static final int LINE_WIDTH = PLAQUE_HEIGHT / 16;




    public static void main(String[] args) {
        StationNamesRockTown msn = new StationNamesRockTown();
        msn.tag("cth").color(Color.BLACK).stationName("CITY HALL");
        msn.generate(Color.RED, Color.YELLOW, Color.GREEN);
    }

    StationNamesRockTown() {

    }

    public StationNamesRockTown transparentColor(Color transparentColor) { this.transparentColor = transparentColor; return this;}
    public StationNamesRockTown color(Color color) { this.color = color; return this;}
    public StationNamesRockTown tag(String tag) { this.tag = tag; return this;}
    public StationNamesRockTown stationName(String stationName) { this.stationName = stationName; return this;}

    public void generate(Color line1Color, Color line2Color, Color line3Color) {
        XCanvas canvas = new XCanvas();
        canvas.height(PLAQUE_HEIGHT).width(PLAQUE_WIDTH);
        //canvas.transparentLayer(transparentColor);
        canvas.begin();

        XRectangleBase rect = new XRectangleFill();
        rect.color(Color.WHITE).left(LINE_WIDTH*2).top(LINE_WIDTH*2).width(PLAQUE_WIDTH - LINE_WIDTH*4).height(PLAQUE_HEIGHT -LINE_WIDTH*4 );
        rect.draw(canvas);

        XFont font = new XFont();
        font.size(FONT_SIZE).center(PLAQUE_WIDTH/2,PLAQUE_HEIGHT/2).color(color).text(stationName).fontName("Arial");
        font.draw(canvas);


        XRoundRectEmpty frame = new XRoundRectEmpty();
        frame.left(LINE_WIDTH*2).top(LINE_WIDTH*2).width(PLAQUE_WIDTH - LINE_WIDTH*4).height(PLAQUE_HEIGHT -LINE_WIDTH*4 );
        frame.arcWidth(LINE_WIDTH*3).arcHeight(LINE_WIDTH*3);
        frame.color(line1Color).lineWidth(LINE_WIDTH);
        frame.draw(canvas);

        if (line2Color != null) {
            frame.left(LINE_WIDTH).top(LINE_WIDTH).width(PLAQUE_WIDTH - LINE_WIDTH*2).height(PLAQUE_HEIGHT -LINE_WIDTH*2 );
            frame.arcWidth(LINE_WIDTH*5).arcHeight(LINE_WIDTH*5);
            frame.color(line2Color);
            frame.draw(canvas);

            if (line3Color != null) {
                frame.left(LINE_WIDTH*3).top(LINE_WIDTH*3).width(PLAQUE_WIDTH - LINE_WIDTH*6).height(PLAQUE_HEIGHT -LINE_WIDTH*6 );
                frame.arcWidth(LINE_WIDTH * 1).arcHeight(LINE_WIDTH * 1);
                frame.color(line3Color);
                frame.draw(canvas);
            }
        }
        canvas.end();

        StringBuilder sb = new StringBuilder();
        sb      .append(tag)
                .append(".png");
        canvas.save(sb.toString(), "png");


    }
}
