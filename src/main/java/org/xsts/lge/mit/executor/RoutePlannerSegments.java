package org.xsts.lge.mit.executor;

import org.xsts.lge.mit.draw.XCanvas;
import org.xsts.lge.mit.draw.XDiscBase;
import org.xsts.lge.mit.draw.XDiscFill;
import org.xsts.lge.mit.draw.XRectangleBase;
import org.xsts.lge.mit.draw.XRectangleFill;

import java.awt.*;

public class RoutePlannerSegments {

    Color color;
    int tenthSize;
    String tag;
    Color transparentColor = new Color(192,192,192);

    public static void main(String [] args) {
        int [] sizeList = {1,2,3,4,5,6,7,8,9,10,12,14,16,18,20, 24, 28,32 };
        Color[] colorList = {
                Color.BLUE, Color.RED, Color.WHITE, Color.BLACK,
                Color.DARK_GRAY, Color.GRAY, Color.LIGHT_GRAY,
                Color.CYAN, Color.GREEN, Color.MAGENTA, Color.ORANGE, Color.PINK};
        String[] tagList = {"blue", "red", "white", "black",
                            "darkgray","gray","lightgray",
                            "cyan","green", "magenta", "orange", "pink"};

        //generateImageSetCLSL(tagList,colorList,sizeList);
        //generateImageSet("blue",Color.BLUE,sizeList);
        /*
        rps.color(Color.BLUE).tag("blue").generate();
        rps.color(Color.RED).tag("red").generate();
        rps.color(Color.YELLOW).tag("yellow").generate();
        rps.color(Color.GREEN).tag("green").generate();
        rps.color(Color.MAGENTA).tag("magenta").generate();
        rps.color(Color.BLACK).tag("black").generate();
        rps.color(Color.ORANGE).tag("orange").generate();
        rps.color(Color.CYAN).tag("cyan").generate();
        rps.color(Color.PINK).tag("pink").generate();*/


    }

    public static void generateImageSetCSL(String tag, Color color, int [] sizeList) {
        RoutePlannerSegments rps = new RoutePlannerSegments();
        rps.color(color).tag(tag);
        for (int size : sizeList) {
            rps.tenthSize(size).generate();
        }
    }

    public static void generateImageSetCLSL(String[] tags, Color[] colors, int [] sizeList) {
        RoutePlannerSegments rps = new RoutePlannerSegments();
        if (tags.length != colors.length) {
            System.err.println("Bad tag-color  list match. Bailing out!");
            System.exit(1);
        }

        for (int pos = 0; pos < tags.length; pos++) {
            rps.color(colors[pos]).tag(tags[pos]);
            for (int size : sizeList) {
                rps.tenthSize(size).generate();
            }
        }
    }


    public static void generateImageSetCLSL(String[] tags, Color[] colors, int  size) {
        RoutePlannerSegments rps = new RoutePlannerSegments();
        if (tags.length != colors.length) {
            System.err.println("Bad tag-color  list match. Bailing out!");
            System.exit(1);
        }

        for (int pos = 0; pos < tags.length; pos++) {
            rps.color(colors[pos]).tag(tags[pos]);
            rps.tenthSize(size).generate();
        }
    }

    public RoutePlannerSegments() {

    }

    public RoutePlannerSegments transparentColor(Color transparentColor) { this.transparentColor = transparentColor; return this;}

    public RoutePlannerSegments color(Color color) { this.color = color; return this;}
    public RoutePlannerSegments tenthSize(int tenthSize) { this.tenthSize = tenthSize; return this;}
    public RoutePlannerSegments tag(String tag) { this.tag = tag; return this;}

    public void generate() {
        generateVerticalStartStop();
        generateVerticalSegment();
        generateVerticalIntermediaryStop();
        generateVerticalDots();
        generateVerticalEndStop();
    }

    private void generateVerticalStartStop() {
        XCanvas canvas = new XCanvas();
        canvas.height(tenthSize*10).width(tenthSize*10);
        canvas.transparentLayer(transparentColor);
        canvas.begin();

        XDiscBase disc = new XDiscFill();
        disc.color(color).center(tenthSize*5, tenthSize * 5).radius(tenthSize*3).lineWidth(0);
        disc.draw(canvas);

        XRectangleBase rect = new XRectangleFill();
        rect.color(color).left(tenthSize*4).top(tenthSize * 5).width(tenthSize*2).height(tenthSize*5).lineWidth(0);
        rect.draw(canvas);



        canvas.end();

        StringBuilder sb = new StringBuilder();
        sb      .append(tag)
                .append("-")
                .append(tenthSize)
                .append("px")
                .append("-")
                .append("vertical")
                .append("-")
                .append("start")
                .append(".png");
        canvas.save(sb.toString(), "png");
    }

    private void generateVerticalSegment() {
        XCanvas canvas = new XCanvas();
        canvas.height(tenthSize*10).width(tenthSize*10);
        canvas.transparentLayer(transparentColor);
        canvas.begin();

        XRectangleBase rect = new XRectangleFill();
        rect.color(color).left(tenthSize*4).top(tenthSize * 0).width(tenthSize*2).height(tenthSize*10).lineWidth(0);
        rect.draw(canvas);

        canvas.end();

        StringBuilder sb = new StringBuilder();
        sb      .append(tag)
                .append("-")
                .append(tenthSize)
                .append("px")
                .append("-")
                .append("vertical")
                .append("-")
                .append("seg")
                .append(".png");
        canvas.save(sb.toString(), "png");
    }

    private void generateVerticalIntermediaryStop() {
        XCanvas canvas = new XCanvas();
        canvas.height(tenthSize*10).width(tenthSize*10);
        canvas.transparentLayer(transparentColor);
        canvas.begin();

        XDiscBase disc = new XDiscFill();
        disc.color(color).center(tenthSize*5, tenthSize * 5).radius(tenthSize*3).lineWidth(0);
        disc.draw(canvas);

        XRectangleBase rect = new XRectangleFill();
        rect.color(color).left(tenthSize*4).top(tenthSize * 0).width(tenthSize*2).height(tenthSize*10).lineWidth(0);
        rect.draw(canvas);

        canvas.end();

        StringBuilder sb = new StringBuilder();
        sb      .append(tag)
                .append("-")
                .append(tenthSize)
                .append("px")
                .append("-")
                .append("vertical")
                .append("-")
                .append("intermediary")
                .append(".png");
        canvas.save(sb.toString(), "png");
    }

    private void generateVerticalDots() {
        XCanvas canvas = new XCanvas();
        canvas.height(tenthSize*10).width(tenthSize*10);
        canvas.transparentLayer(transparentColor);
        canvas.begin();

        XRectangleBase rect = new XRectangleFill();
        rect.color(color).left(tenthSize*4).top(tenthSize * 1).width(tenthSize*2).height(tenthSize*2).lineWidth(0);
        rect.draw(canvas);

        XRectangleBase rect2 = new XRectangleFill();
        rect2.color(color).left(tenthSize*4).top(tenthSize * 4).width(tenthSize*2).height(tenthSize*2).lineWidth(0);
        rect2.draw(canvas);

        XRectangleBase rect3 = new XRectangleFill();
        rect3.color(color).left(tenthSize*4).top(tenthSize * 7).width(tenthSize*2).height(tenthSize*2).lineWidth(0);
        rect3.draw(canvas);

        canvas.end();

        StringBuilder sb = new StringBuilder();
        sb      .append(tag)
                .append("-")
                .append(tenthSize)
                .append("px")
                .append("-")
                .append("vertical")
                .append("-")
                .append("dots")
                .append(".png");
        canvas.save(sb.toString(), "png");
    }

    private void generateVerticalEndStop() {
        XCanvas canvas = new XCanvas();
        canvas.height(tenthSize*10).width(tenthSize*10);
        canvas.transparentLayer(transparentColor);
        canvas.begin();

        XDiscBase disc = new XDiscFill();
        disc.color(color).center(tenthSize*5, tenthSize * 5).radius(tenthSize*3).lineWidth(0);
        disc.draw(canvas);

        XRectangleBase rect = new XRectangleFill();
        rect.color(color).left(tenthSize*4).top(tenthSize * 0).width(tenthSize*2).height(tenthSize*5).lineWidth(0);
        rect.draw(canvas);

        canvas.end();

        StringBuilder sb = new StringBuilder();
        sb      .append(tag)
                .append("-")
                .append(tenthSize)
                .append("px")
                .append("-")
                .append("vertical")
                .append("-")
                .append("end")
                .append(".png");
        canvas.save(sb.toString(), "png");
    }

}
