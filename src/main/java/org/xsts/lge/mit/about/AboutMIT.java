/*
 * Group : XSTS/LGE
 * Project : Minimalist Image Toolkit
 * Created and maintained since 2016 by the Open Initiative for Information about Public Transportation
 */
package org.xsts.lge.mit.about;

public class AboutMIT {
    public static void main(String[] args) {
        System.out.println("Minimalist Image Toolkit");
    }
}
